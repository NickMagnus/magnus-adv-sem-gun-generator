﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DisplayFloatValueFromEnum : MonoBehaviour
{
    public enum FloatToDisplay {
    bulletSpeed,
    shotsPerSecond,
    bulletLifetime,
    damagePerBullet,
    gunshotPitch,
    reloadTime,
    percentSlow,
    ammoPerClip,
    };
    public FloatToDisplay floatEnum;

    public string defaultText;
    float currentGunBulletSpeed;
    public Text textObj;
    GameObject playerReference;

    // this script reads floats from currentGun and displays them. Choose what value to display in editor
    void Start()
    {
        playerReference = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        CopyAndDisplayValuesFromCurrentGun();
    }
    void CopyAndDisplayValuesFromCurrentGun()
    {
        switch (floatEnum)
        {
            case DisplayFloatValueFromEnum.FloatToDisplay.bulletSpeed:
                float copyBulletSpeed = playerReference.GetComponent<PlayerShooting>().GetCurrentGun().bulletSpeed;
                textObj.text = defaultText + copyBulletSpeed.ToString();
                break;
            case DisplayFloatValueFromEnum.FloatToDisplay.shotsPerSecond:
                float copyShotsPerSecond = playerReference.GetComponent<PlayerShooting>().GetCurrentGun().shotsPerSecond;
                textObj.text = defaultText+ copyShotsPerSecond.ToString();
                break;
            case DisplayFloatValueFromEnum.FloatToDisplay.bulletLifetime:
                float copyBulletLifetime = playerReference.GetComponent<PlayerShooting>().GetCurrentGun().bulletLifetime;
                textObj.text = defaultText + copyBulletLifetime.ToString();
                break;
            case DisplayFloatValueFromEnum.FloatToDisplay.damagePerBullet:
                float copyDamagePerBullet = playerReference.GetComponent<PlayerShooting>().GetCurrentGun().damagePerBullet;
                textObj.text = defaultText + copyDamagePerBullet.ToString();
                break;
            case DisplayFloatValueFromEnum.FloatToDisplay.gunshotPitch:
                float copyGunshotPitch = playerReference.GetComponent<PlayerShooting>().GetCurrentGun().gunshotPitch;
                textObj.text = defaultText + copyGunshotPitch.ToString();
                break;
            case DisplayFloatValueFromEnum.FloatToDisplay.reloadTime:
                float copyReloadTime = playerReference.GetComponent<PlayerShooting>().GetCurrentGun().reloadTime;
                textObj.text = defaultText + copyReloadTime.ToString();
                break;
            case DisplayFloatValueFromEnum.FloatToDisplay.percentSlow:
                float copyPercentSlow = playerReference.GetComponent<PlayerShooting>().GetCurrentGun().speedLimitPercentage;
                textObj.text = defaultText + copyPercentSlow.ToString();
                break;
            case DisplayFloatValueFromEnum.FloatToDisplay.ammoPerClip:
                float copyAmmoPerClip = playerReference.GetComponent<PlayerShooting>().GetCurrentGun().ammoPerClip;
                textObj.text = defaultText + copyAmmoPerClip.ToString();
                break;
            default:
                break;
        }

    }
}
