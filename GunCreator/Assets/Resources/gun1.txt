{
    "tempBulletLifetime": 9.16441822052002,
    "tempBulletSpeed": 50.0,
    "tempAmmoPerClip": 257.0,
    "tempKickback": 80.0,
    "tempShotsPerSecond": 11.628170013427735,
    "tempReloadTime": 2.4228668212890627,
    "tempSpeedLimitPercentage": 26.586347579956056,
    "tempGunshotPitch": 1.8566042184829713,
    "tempIsExplosiveAmmo": false,
    "tempDoesDespawnOnHit": false,
    "tempIsAutomatic": false
}