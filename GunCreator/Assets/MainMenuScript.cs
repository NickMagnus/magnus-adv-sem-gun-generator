﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    public GameObject HelpCanvas;
    public GameObject MenuCanvas;
    public GameObject[] menuButtons;
    public GameObject BackToMenuButton;

    public void FuncQuit()
    {
        Application.Quit();
    }

    public void FuncStartGame()
    {
        SceneManager.LoadScene(1);//load the main scene, make sure to have main menu in build order (0)
    }

    public void OpenHelpCanvas()
    {
        HelpCanvas.SetActive(true);
        foreach(GameObject obj in menuButtons)
        {
            obj.SetActive(false);
        }
        BackToMenuButton.SetActive(true);
    }
    public void OpenMainMenuCanvas()
    {
        HelpCanvas.SetActive(false);
        foreach (GameObject obj in menuButtons)
        {
            obj.SetActive(true);
        }
        BackToMenuButton.SetActive(false);
    }

}
