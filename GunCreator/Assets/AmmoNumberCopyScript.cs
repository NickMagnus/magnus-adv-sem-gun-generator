﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmmoNumberCopyScript : MonoBehaviour
{
    Text textReference;
    public GameObject playerRef;
    // Start is called before the first frame update
    void Start()
    {
        textReference = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        int ammo = (int)playerRef.GetComponent<PlayerShooting>().GetCurrentGun().GetCurrentAmmo();
        textReference.text = ammo.ToString();
    }
    private void LateUpdate()
    {
        Quaternion playerRot = playerRef.transform.rotation;
        print("Player rot = " + playerRot);
        Quaternion newRot = new Quaternion(-playerRot.x, -playerRot.y, -playerRot.z, playerRot.w);
        transform.localRotation = newRot;
        print("canvas rot = " + transform.rotation);
    }
}
