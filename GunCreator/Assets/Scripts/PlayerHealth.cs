﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerHealth : MonoBehaviour
{
    // Start is called before the first frame update
    public int health = 100;

    void Start()
    {
        GameManager.GetGM.ChangePlayerHealth(health);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TakeDamage(int _dmg)
    {
        health -= _dmg;
        GameManager.GetGM.ChangePlayerHealth(health);
        //play hit sound

        GetComponentInChildren<SpriteRenderer>().enabled = false;
        Invoke("ReEnableSpriteRend", .05f);

        if (health < 0)
        {
            Die();
        }

    }
        void Die()
        {
            //put some feedback here
            GameObject.FindObjectOfType<GameManager>().RestartGame();
            // Destroy(gameObject);
        }
    void ReEnableSpriteRend()
    {
        GetComponentInChildren<SpriteRenderer>().enabled = true;
    }

}
