﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEntityScript : MonoBehaviour
{
    AudioSource audi;
    // Start is called before the first frame update
    void Start()
    {

    }

    public void PlayAudio(AudioClip clip, float pitch, float volumeScale)
    {
        audi = GetComponent<AudioSource>();
        audi.volume = volumeScale;
        audi.pitch = pitch;
        audi.PlayOneShot(clip, volumeScale);
        print("BANG");
        Destroy(gameObject, clip.length);
    }
}
