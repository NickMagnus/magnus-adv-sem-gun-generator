﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public static GameManager instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.
    public static GameManager GetGM { get { return instance; } }

    public Text playerHealth;
    public Text playerKills;

    int numPlayerKills;
    float numPlayerKillsFloat;

    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        //DontDestroyOnLoad(gameObject);
    }
    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }

    public void ChangePlayerHealth(int health)
    {
        playerHealth.text = "Health: " + health.ToString();
    }

    public void IncrementPlayerKillsUI(int numKillsToAdd)
    {
        numPlayerKills += numKillsToAdd;
        playerKills.text = "Kills: " + numPlayerKills.ToString();
    }
    public void IncrementPlayerKillsUI(float numKillsToAddFloat)
    {
        numPlayerKillsFloat += numKillsToAddFloat;
        playerKills.text = "Kills: " + numPlayerKillsFloat.ToString();
    }
}


