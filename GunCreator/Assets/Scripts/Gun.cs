﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
   
    bool fireGun = false;
    bool isReloading = false;
    float reloadTimer;
    #region gunSettings
    [Space]
    [Header("Ammo Type Settings")]
    [Range(1, 20)]
    public float bulletLifetime;
    [Range(1, 50)]
    public float bulletSpeed;
    [Range(1, 500)]
    public float ammoPerClip;
    public bool isExplosiveAmmo;
    public bool doesDespawnOnHit;
    [Range(1, 500)]
    public float kickbackForcePerShot;
    [Space]
    [Space]
    [Header("Fire Rate Settings")]
    [Range(1, 40)]
    public float shotsPerSecond;
    float timeSinceLastShot = 0f;
    public bool isAutomatic;
    
    [Range(.25f, 5)]
    public float reloadTime;
    [Space]
    [Space]
    [Header("Damage Settings")]
    [Range(1, 100)]
    public int damagePerBullet;
    [Range(1, 100)]
    public float damagePercentDropOff;//at the tail end of the bulletLifeTime, how much percent of the damage has fell off
    [Space]
    [Space]
    [Header("Weight Settings")]
    [Range(1, 50)]
    public float speedLimitPercentage;//how much slower the player moves while equipping this gun
    [Space]
    [Space]
    [Header("Weight Settings")]
    [Range(0.5f, 2f)]
    public float gunshotPitch;
    [Range(0.5f, 2f)]
    #endregion


    public GameObject bulletPrefab;
    Transform bulletSpawnTransform;
    float currentClip;
    public GameObject AudioEntity;
    public AudioClip gunShotSound;//probably want to move these to bullet and pass it in
    public AudioClip reloadSound;


    void Start()
    {
        currentClip = ammoPerClip;
    }

    
    void Update()
    {
      if (isReloading)
      {
         ReloadGun();
      }
      else if (fireGun)
      {
          if (timeSinceLastShot > 1 / shotsPerSecond)
          {
              funcShoot();
              timeSinceLastShot = 0;
          }
      }
        //print("AAA" + gameObject.name);
        timeSinceLastShot += Time.deltaTime;

    }

    void funcShoot()
    {
     if (currentClip > 0)
       {
         fireGun = isAutomatic; //if we are not an automatic gun, fire one shot than stop
         GameObject newBul1 = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
         newBul1.transform.rotation = gameObject.transform.rotation;
         newBul1.gameObject.GetComponent<BulletScript>().entityThatShotThisBullet = GameObject.FindGameObjectWithTag("Player");
         newBul1.GetComponent<BulletScript>().InitBullet(this);
         currentClip -= 1;
         GameObject newSound = Instantiate(AudioEntity, gameObject.transform.position, Quaternion.identity);
         newSound.GetComponent<SoundEntityScript>().PlayAudio(gunShotSound, gunshotPitch, .25f);

         float screenShakeForceCalc = (damagePerBullet * 1.35f) / shotsPerSecond;
         float screenShakeValue = Mathf.Min(screenShakeForceCalc, 10);
         screenShakeValue = Mathf.Max(screenShakeValue, 1.75f);
         

            Camera.main.GetComponent<CameraLogic>().triggerCameraShake(.1f, screenShakeValue);
       }
       else{
            isReloading = true;
       }
        
        //print("fire");
    }
    #region setters
    public void setBulletLifetime(float newLifetime)
    {
        if (newLifetime > 20) //TODO, MAGIC NUMBER BUT SHOULD BE MAX_BULLETLIFETIME
        {
            print("newLifeTime too hight, clamping to 20");
            bulletLifetime = 20;
        }
        else
        {
            bulletLifetime = newLifetime;
        }
    }

    public float GetCurrentAmmo()
    {
        return currentClip;
    }

    public void setBulletSpeed(float newSpeed)
    {
    if (newSpeed > 50)
         {
            print("newSpeed too hight, clamping to 20");
            bulletSpeed = 20;
        }
         else
         {
           bulletSpeed = newSpeed;
         }
    }
    public void setFireGun(bool shouldFire)
    {
        fireGun = shouldFire;
    }
    public void setExplosiveAmmo(bool isExplosive)
    {
        isExplosiveAmmo = isExplosive;
    }
    //update clip ammo
    public void setAmmoPerClip(int newAmmo)
    {
        ammoPerClip = newAmmo;
        currentClip = newAmmo;
        ManualReloadGun();
    }
    public void setAmmoPerClip(float newAmmo)
    {
        int temp = Mathf.RoundToInt(newAmmo);
        ammoPerClip = temp;
        currentClip = temp;
        ManualReloadGun();
    }

    public void setKickbackPerShot(float newKickback)
    {
        kickbackForcePerShot = newKickback;
    }
    public void setShotsPerSecond(float newShots)
    {
        shotsPerSecond = newShots;
    }
    public void setReloadTime(float newTime)
    {
        reloadTime = newTime;
    }
    public void setSpeedLimitPercentage(float newPercent)
    {
        speedLimitPercentage = newPercent;
    }
    public void setGunshotPitch(float newPitch)
    {
        gunshotPitch = newPitch;
    }
    public void setDoesDespawnOnHit(bool newDoesDespawn)
    {
        doesDespawnOnHit = newDoesDespawn;
    }
    public void setIsAuto(bool newIsAuto)
    {
        isAutomatic = newIsAuto;
    }

    #endregion
    void ReloadGun()
    {
        if (reloadTimer == 0)
        {
            GameObject newSound = Instantiate(AudioEntity, gameObject.transform.position, Quaternion.identity);
            //do some math to change length of sound to equal reload time
            newSound.GetComponent<SoundEntityScript>().PlayAudio(reloadSound, 1.0f, .15f);
        }
        reloadTimer += Time.deltaTime;
        //print("RELOADING");
        if (reloadTimer > reloadTime)
          {
            currentClip = ammoPerClip;
            isReloading = false;
            reloadTimer = 0;
            print(currentClip);
          }
    }

    public void ManualReloadGun()
    {
    if (currentClip == ammoPerClip)
        print("No reloading with full ammo");
    else
       isReloading = true;
    }
}
