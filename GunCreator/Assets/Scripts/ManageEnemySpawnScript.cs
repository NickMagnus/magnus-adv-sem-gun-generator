﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageEnemySpawnScript : MonoBehaviour
{
    
    public List<GameObject> spawnPosList = new List<GameObject>();
    public GameObject enemyPrefab;
    public float timeBetweenEnemySpawns;
    public float maxSpawnTimeDeviation;

    float timeSinceLastSpawn = 0;
    float currentDeviation;
    void Start()
    {
        currentDeviation = Random.Range(-maxSpawnTimeDeviation, maxSpawnTimeDeviation);
    }

    // Update is called once per frame
    void Update()
    {
        timeSinceLastSpawn += Time.deltaTime;
        if (timeSinceLastSpawn > timeBetweenEnemySpawns + currentDeviation)
        {
            //spawn enemy
            //print("Spawning");
            currentDeviation = Random.Range(-maxSpawnTimeDeviation, maxSpawnTimeDeviation);
            timeSinceLastSpawn = 0;
            int spawnPosIndex = Random.Range(0, spawnPosList.Count);
            GameObject newEnemy = Instantiate(enemyPrefab, spawnPosList[spawnPosIndex].transform.position, Quaternion.identity);
       
            if (timeBetweenEnemySpawns > .6)
            {
              timeBetweenEnemySpawns *= .95f;
            }
            if (maxSpawnTimeDeviation > .6)
            {
                maxSpawnTimeDeviation *= .95f;
            }

        }
    }
}
