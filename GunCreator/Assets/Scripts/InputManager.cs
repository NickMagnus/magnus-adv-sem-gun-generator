﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class InputManager : MonoBehaviour
{
    GameObject PlayerReference;
    PlayerMovement pMovement;
    PlayerShooting pShoot;
    GameObject CustomizationCanvas;
    bool isCustomCanvasActive = true;
   
    // Start is called before the first frame update
    void Start()
    {
        PlayerReference = GameObject.Find("Player");
        CustomizationCanvas = GameObject.Find("CustomizationCanvas");
        if (PlayerReference == null)
        {
            throw new System.Exception("Major Error, Player not found in Input Manager");
        }
        pMovement = PlayerReference.GetComponent<PlayerMovement>();
        pShoot    = PlayerReference.GetComponent<PlayerShooting>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckPlayerMoveInput();
        CheckPlayerShootInput();
        CheckPauseInput();
        CheckCustomizationUIInput();
       // print(CustomizationCanvas.activeInHierarchy);
    }

    void CheckCustomizationUIInput()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {             
            CustomizationCanvas.SetActive(!isCustomCanvasActive);
            isCustomCanvasActive = !isCustomCanvasActive;
        }
    }

    void CheckPlayerMoveInput()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            pMovement.up = true;
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            pMovement.left = true;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            pMovement.down = true;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            pMovement.right = true;
        }

        if (Input.GetKeyUp(KeyCode.W))
        {
            pMovement.up = false;
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            pMovement.left = false;
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            pMovement.down = false;
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            pMovement.right = false;
        }
    }

    private void CheckPlayerShootInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
          if (!EventSystem.current.IsPointerOverGameObject()) //prevent gun from being fired while clicking on a button
            {
            pShoot.Shoot(true);        
          }
        }
        if (Input.GetMouseButtonUp(0))
        {
           pShoot.Shoot(false);
        }
    }

    void CheckPauseInput()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            // print("pause game not implemented");
            SceneManager.LoadScene(0);
        }
    }
}
