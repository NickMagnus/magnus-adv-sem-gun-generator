﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{

    public float oldBulletSpeed = 16f;
    public int BulletDamage = 1;
    public float TimeScale = 1.0f;

    public GameObject entityThatShotThisBullet;
    GameObject PlayerReference;

    //public GameObject explosionPrefab;
    //public GameObject muzzleFlashPrefab;

    //values that the bullet needs to pull from the gun
    float bulletLifetime;
    float bulletSpeed;
    bool isExplosiveAmmo;
    bool doesDespawnOnHit;
    int bulletDamage;


    bool isInit = false;

    void Start()
    {
        PlayerReference = GameObject.Find("Player");
        if (PlayerReference == null)
        {
            throw new System.Exception("Fatal Error, Player not found in Input Manager");
        }
        Destroy(gameObject, 6f);//temporary override to cleanup
    }

    // Update is called once per frame
    void Update()
    {
        if (isInit)
        {
            Move();
        }     
    }

    void Move()
    {
        transform.Translate(-1 * Vector2.left * bulletSpeed * (Time.deltaTime * TimeScale));
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        //collision with enemy
        if (col.gameObject.tag == "Enemy" && entityThatShotThisBullet != col.gameObject)
        {
            //deal damage
            //print("Bullet Dealign Damage");
            col.GetComponent<EnemyScript>().TakeDamage(bulletDamage);

            if (isExplosiveAmmo)
            {
                //Instantiate(explosionPrefab, transform.position, Quaternion.identity);
            }
            if (doesDespawnOnHit)
            {
                //Destroy(gameObject);
                BulletCull();
            }
            
         }
        else if (col.gameObject.tag == "Player" && entityThatShotThisBullet != col.gameObject)
        {
            //deal damage
            //print("Bullet Dealign Damage to player");
            col.GetComponent<PlayerHealth>().TakeDamage(bulletDamage);

            if (isExplosiveAmmo)
            {
                //Instantiate(explosionPrefab, transform.position, Quaternion.identity);
            }
            if (doesDespawnOnHit)
            {
                //Destroy(gameObject);
                BulletCull();
            }
        }

        //write collision with other bullets

    }

    void BulletCull()
    {
       if (isExplosiveAmmo)
       {
        //Instantiate(explosionPrefab, transform.position, Quaternion.identity);
       }
        Destroy(gameObject);
    }

    public void InitBullet(Gun parentGun)
    {
        bulletLifetime      = parentGun.bulletLifetime;
        bulletSpeed         = parentGun.bulletSpeed;
        isExplosiveAmmo     = parentGun.isExplosiveAmmo;
        doesDespawnOnHit    = parentGun.doesDespawnOnHit;
        bulletDamage        = parentGun.damagePerBullet;
        
        isInit = true;
        Invoke("BulletCull", bulletLifetime);

        float scaleMultiplier = 1 + (bulletDamage / 100.0f);
        transform.localScale *= scaleMultiplier; 

    }

    public void EnemyInitBullet()
    {
        bulletLifetime = 2.0f;
        bulletSpeed = 15;
        isExplosiveAmmo = false;
        doesDespawnOnHit = true;
        bulletDamage = 4;


       isInit = true;
    }
}
