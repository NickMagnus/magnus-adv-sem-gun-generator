﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerShooting : MonoBehaviour
{
    public GameObject Bullet1Prefab;
    public bool ShouldShoot = false;
    public Transform BulletSpawnLocation;
    public List<GameObject> gunList = new List<GameObject>();
    Gun currentGun;

    int numPlayerKills;

    public Color[] gunColorList;
    public SpriteRenderer gunSpriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        numPlayerKills = 0;
        GameManager.GetGM.IncrementPlayerKillsUI(numPlayerKills);
        for (int i = 0; i < gunList.Count; i++)
        {
         GameObject Gun = Instantiate(gunList[i], gameObject.transform.position, Quaternion.identity);
            Gun.transform.parent = gameObject.transform;
            Gun.name = "Gun" + i.ToString();
            gunList[i] = Gun;
            Gun.SetActive(false);
        }
        LoadAllGuns();

        gunSpriteRenderer.color = gunColorList[0];
    }

    void LoadAllGuns()
    {
        HandleTextFile loader = GameObject.Find("[GameManager]").GetComponent<HandleTextFile>();

        //swap through all guns and load manually, load first gun afterwards
        for (int i = gunList.Count -1; i > -1; i--)
        {
          currentGun = gunList[i].GetComponent<Gun>();
          loader.ForceLoad();
        }
        currentGun.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        CheckSwitchGunInput();

        if (Input.GetKeyDown(KeyCode.R))
        {
            currentGun.ManualReloadGun();
        }
    }

    void CheckSwitchGunInput()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            currentGun.gameObject.SetActive(false);
            currentGun = gunList[0].GetComponent<Gun>();
            currentGun.gameObject.SetActive(true);

            gunSpriteRenderer.color = gunColorList[0];
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            currentGun.gameObject.SetActive(false);
            currentGun = gunList[1].GetComponent<Gun>();
            currentGun.gameObject.SetActive(true);

            gunSpriteRenderer.color = gunColorList[1];
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            currentGun.gameObject.SetActive(false);
            currentGun = gunList[2].GetComponent<Gun>();
            currentGun.gameObject.SetActive(true);

            gunSpriteRenderer.color = gunColorList[2];
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            currentGun.gameObject.SetActive(false);
            currentGun = gunList[3].GetComponent<Gun>();
            currentGun.gameObject.SetActive(true);

            gunSpriteRenderer.color = gunColorList[3];
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            currentGun.gameObject.SetActive(false);
            currentGun = gunList[4].GetComponent<Gun>();
            currentGun.gameObject.SetActive(true);

            gunSpriteRenderer.color = gunColorList[4];
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            currentGun.gameObject.SetActive(false);
            currentGun = gunList[5].GetComponent<Gun>();
            currentGun.gameObject.SetActive(true);

            gunSpriteRenderer.color = gunColorList[5];
        }
        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            currentGun.gameObject.SetActive(false);
            currentGun = gunList[6].GetComponent<Gun>();
            currentGun.gameObject.SetActive(true);

            gunSpriteRenderer.color = gunColorList[6];
        }
        if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            currentGun.gameObject.SetActive(false);
            currentGun = gunList[7].GetComponent<Gun>();
            currentGun.gameObject.SetActive(true);

            gunSpriteRenderer.color = gunColorList[7];
        }
        if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            currentGun.gameObject.SetActive(false);
            currentGun = gunList[8].GetComponent<Gun>();
            currentGun.gameObject.SetActive(true);

            gunSpriteRenderer.color = gunColorList[8];
        }
    }

    public int GetCurrentGunIndex()
    {
        return gunList.IndexOf(currentGun.gameObject);
    }

    public void Shoot(bool shouldShoot)
    {
        currentGun.setFireGun(shouldShoot);
    }

    public Gun GetCurrentGun()
    {
        return currentGun;
    }

    public void setBulletLifetime(float newLifetime)
    {
        if (newLifetime > 20) //TODO, MAGIC NUMBER BUT SHOULD BE MAX_BULLETLIFETIME
        {
            print("newLifeTime too hight, clamping to 20");
            currentGun.bulletLifetime = 20;
        }
        else
        {
            currentGun.bulletLifetime = newLifetime;
        }
    }
    public void setBulletSpeed(float newSpeed)
    {
        if (newSpeed > 50)
        {
            print("newSpeed too hight, clamping to 20");
            currentGun.bulletSpeed = 50;
        }
        else if (currentGun != null)
        {
            currentGun.bulletSpeed = newSpeed;
        }
    }
    public void setExplosiveAmmo(bool isExplosive)
    {
        currentGun.isExplosiveAmmo = isExplosive;
    }
    //update clip ammo
    public void setAmmoPerClip(int newAmmo)
    {
        currentGun.ammoPerClip = newAmmo;
        currentGun.ManualReloadGun();
    }

    public void setKickbackPerShot(float newKickback)
    {
        currentGun.kickbackForcePerShot = newKickback;
    }

    public void setIsAuto()
    {
        currentGun.isAutomatic = !currentGun.isAutomatic;
    }





    ///OVERLOAD FUNCTIONS WITH SLIDER INPUTS
    ///

    public void setBulletLifetime(Slider slider)
    {
        if (slider.value > 20) //TODO, MAGIC NUMBER BUT SHOULD BE MAX_BULLETLIFETIME
        {
            print("newLifeTime too hight, clamping to 20");
            currentGun.bulletLifetime = 20;
        }
        else if (currentGun != null)
        {
            currentGun.bulletLifetime = slider.value;
        }
    }
    public void setBulletSpeed(Slider slider)
    {
        if (slider.value > 50)
        {
            print("newSpeed too hight, clamping to 20");
            currentGun.bulletSpeed = 50;
        }
        else if (currentGun != null)
        {
            currentGun.bulletSpeed = slider.value;
        }
    }

    public void setShotsPerSecond(Slider slider)
    {
        if (slider.value > 40)
        {
            currentGun.shotsPerSecond = 40;
        }
        else if (currentGun != null)
        {
            currentGun.shotsPerSecond = slider.value;
        }
    }

    public void setDamagerPerBullet(Slider slider)
    {
        if (slider.value > 100)
        {
            currentGun.damagePerBullet = 100;
        }
        else if (currentGun != null)
        {
            currentGun.damagePerBullet = (int)slider.value;
        }
    }
    public void setSpeedCutPercentage(Slider slider)
    {
        if (slider.value > 50)
        {
            currentGun.speedLimitPercentage = 100;
        }
        else if (currentGun != null)
        {
            currentGun.speedLimitPercentage = slider.value;
        }
    }

    //update clip ammo
    public void setAmmoPerClip(Slider slider)
    {
        if (slider.value > 500)
        {
            currentGun.ammoPerClip = 500;
        }
        else if (currentGun != null)
        {
            currentGun.ammoPerClip = slider.value;
            currentGun.ManualReloadGun();
        } 
    }

    public void setReloadTime(Slider slider)
    {
        if (slider.value > 5)
        {
            currentGun.reloadTime = 5;
        }
        else if (currentGun != null)
        {
            currentGun.reloadTime = slider.value;
        }
    }

    public void setGunshotPitch(Slider slider)
    {
        if (slider.value > 2.0f)
        {
            currentGun.gunshotPitch = 2;
        }
        else if (currentGun != null)
        {
            currentGun.gunshotPitch = slider.value;
        }
    }
}
