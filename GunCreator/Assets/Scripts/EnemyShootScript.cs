﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShootScript : MonoBehaviour
{
    public GameObject Bullet1Prefab;
    public Transform BulletSpawnLocation;
    public List<GameObject> gunList = new List<GameObject>(); //we can keep this gun list synced with the player, maybe choose a random gun the player made
                                                              // Start is called before the first frame update
    public GameObject AudioEntity;
    public AudioClip gunShotSound;//probably want to move these to bullet and pass it in

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Shoot()
    {
        GameObject newBul1 = Instantiate(Bullet1Prefab, BulletSpawnLocation.position, Quaternion.identity);
        newBul1.transform.rotation = gameObject.transform.rotation;
        newBul1.gameObject.GetComponent<BulletScript>().entityThatShotThisBullet = gameObject;
        newBul1.GetComponent<BulletScript>().EnemyInitBullet();

        GameObject newSound = Instantiate(AudioEntity, gameObject.transform.position, Quaternion.identity);
        newSound.GetComponent<SoundEntityScript>().PlayAudio(gunShotSound, 1.0f, .25f);
    }
}
