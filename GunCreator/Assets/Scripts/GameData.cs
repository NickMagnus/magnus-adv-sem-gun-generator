﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData
{
    public float tempBulletLifetime, tempBulletSpeed, tempAmmoPerClip, tempKickback, tempShotsPerSecond;
    public float tempReloadTime, tempSpeedLimitPercentage, tempGunshotPitch;
    public bool  tempIsExplosiveAmmo, tempDoesDespawnOnHit, tempIsAutomatic;
}
