﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

   public bool up;
   public bool down;
   public bool left;
   public bool right;

    public float maxSpeed;
    public float accelaration;
    float moveSpeed;


    // Start is called before the first frame update
    void Start()
    {
        moveSpeed = maxSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        float moveRatio = GetComponent<PlayerShooting>().GetCurrentGun().speedLimitPercentage / 100;
        moveSpeed = maxSpeed * (1 - moveRatio);
        Move();
        AimAtMouse();
    }




    //take input from input manager and move accordingly
    void Move()
    {
        if (up)
        {
            transform.Translate(Vector2.up * moveSpeed * Time.deltaTime, Space.World);
        }
        if (right)
        {
            transform.Translate(Vector2.right * moveSpeed * Time.deltaTime, Space.World);
        }
        if (left)
        {
            transform.Translate(Vector2.left * moveSpeed * Time.deltaTime, Space.World);
        }
        if (down)
        {
            transform.Translate(Vector2.down * moveSpeed * Time.deltaTime, Space.World);
        }
    }


    void AimAtMouse()
    {
        Vector3 dir = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}
