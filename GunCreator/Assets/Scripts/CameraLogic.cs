﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLogic : MonoBehaviour
{
    public float zoomDistance = 1.5f; //controls how far away the camera is from the player
    public float cameraFollowSpeed = .8f; //how fast will the camera slerp towards the target location
    public float physicsPredictionTime = .5f; //how far ahead in time we look for camera prediction

    [SerializeField]
    private Transform playerTransform; //stores the position of the player

    private Camera thisCam;



    //shake values
    #region shakeValues

    Transform camTransform;

    // How long the object should shake for.
    float shakeDuration = 0.5f;

    // Amplitude of the shake. A larger value shakes the camera harder.
    float shakeAmount = 15f; //default values
    float decreaseFactor = 1.0f;
    public bool shouldShake = false;

    Vector3 originalPos;
    float originalShakeDuration = .5f; //<--add this

    Vector3 previousPositionDifferenceDuringCameraShake;
    #endregion


    void Start()
    {
        thisCam = GetComponent<Camera>();
        camTransform = thisCam.transform;
        originalPos = camTransform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        CameraFollow(thisCam, playerTransform.position, zoomDistance, cameraFollowSpeed, physicsPredictionTime);
       
    }

    public void shakeCameraLogic()
    {
        if (shakeDuration > 0)
        {
            camTransform.localPosition = Vector3.Lerp(camTransform.localPosition, camTransform.localPosition + UnityEngine.Random.insideUnitSphere * shakeAmount, Time.deltaTime * 3);

            shakeDuration -= Time.deltaTime * decreaseFactor;
        }
        else
        {
            shakeDuration = originalShakeDuration; //<--add this
            //camTransform.localPosition = originalPos;
            shouldShake = false;
        }
    }

    public void triggerCameraShake(float _duration, float _power)
    {
        shakeDuration = _duration;
        shakeAmount = _power;
        shouldShake = true;
    }

    void CameraFollow(Camera camera, Vector3 targetPosition, float zoomAmount, float followSpeed, float physicsPreditionTime)
    {
        Vector3 cameraTargetLocation; //the location that the camera tries to keep in the center of the screen

        /*
         tempPos = playerPos + (velocity * physicsPredictionTime)     TODO  
         */

        cameraTargetLocation = targetPosition; //TODO - Physics prediction
        cameraTargetLocation = cameraTargetLocation + ( camera.transform.forward * zoomAmount );



         if (shouldShake)
        {
            
            //shakeCameraLogic();
            if (shakeDuration > 0)
            {

                cameraTargetLocation = Vector3.Lerp(cameraTargetLocation, cameraTargetLocation + (UnityEngine.Random.insideUnitSphere * shakeAmount), Time.deltaTime * 3);

                shakeDuration -= Time.deltaTime * decreaseFactor;

                
            }
            else
            {
                shakeDuration = originalShakeDuration; //<--add this
                //cameraTargetLocation = originalPos;
                shouldShake = false;
            }
        }
        
        camera.transform.position = Vector3.Slerp(camera.transform.position, cameraTargetLocation, followSpeed);






        //This is to avoid oscillation/jiggling around the target position
        if ((targetPosition - camera.transform.position).magnitude <= 0.05f)
        {
            camera.transform.position = targetPosition;
        }
        

    }




}
