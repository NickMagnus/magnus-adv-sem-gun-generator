﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    public int health;

    //add audio source
    // Start is called before the first frame update

    public float timeBetweenShots; //standard time between each shot
    public float maxVariance; //variance between each shot 

    private float currentVariance;
    float timeSinceLastShot = 0f;

    public float EnemyMoveSpeed = .5f;

    public float minDistanceToShoot;
    GameObject playerReference;

    EnemyShootScript eShoot;

    void Start()
    {
        playerReference = GameObject.Find("Player");
        if (playerReference == null)
        {
            throw new System.Exception("Major Error, Player not found in Input Manager");
        }
        currentVariance = Random.Range(-maxVariance, maxVariance);
        eShoot = GetComponent<EnemyShootScript>();
    }

    // Update is called once per frame
    void Update()
    {
        //track player
        //update AI behavior
        if (Vector3.Distance(gameObject.transform.position, playerReference.gameObject.transform.position) > 5)
        {
            LerpToPlayer();
        }
        else if (timeSinceLastShot > timeBetweenShots + currentVariance)
        {
            RunShootingLogic();
        }
        LookAtPlayer();
        timeSinceLastShot += Time.deltaTime;
    }

    void LerpToPlayer()
    {
        //simple path will be removed later
        transform.position = Vector3.Lerp(gameObject.transform.position, playerReference.gameObject.transform.position, Time.deltaTime * EnemyMoveSpeed);
    }

    void RunShootingLogic()
    {
       // print("Shoot NOW!");
        currentVariance = Random.Range(-maxVariance, maxVariance);
        timeSinceLastShot = 0;
        eShoot.Shoot();
    }

    void LookAtPlayer()
    {
        Vector3 lookAt = playerReference.transform.position - transform.position;
        lookAt.Normalize();

        float zRotation = Mathf.Atan2(lookAt.y, lookAt.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, zRotation);
    }

    public void TakeDamage(int _dmg)
    {
        health -= _dmg;
       // print("current " + health);
       // print("max " + health);
       // print(health / health);
        //play hit sound

        GetComponentInChildren<SpriteRenderer>().enabled = false;
        Invoke("ReEnableSpriteRend", .05f);

        if (health < 0)
        {
            Die();
        }

    }
     void Die()
     {
     //put some feedback here
     GameManager.GetGM.IncrementPlayerKillsUI(0.5f);
     Destroy(gameObject);

    }

    void ReEnableSpriteRend()
    {
        GetComponentInChildren<SpriteRenderer>().enabled = true;
    }
}
