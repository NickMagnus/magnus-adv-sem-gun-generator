﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;

public class HandleTextFile : MonoBehaviour
{

    //call read file when loading guns
    //call write file to save stuff (need to take all gun values and pass them in after clearing the file)
    public bool shouldWrite = false;
    public bool shouldRead = false;

    GameData gameData;
    GameObject playerRef;
    private void Start()
    {
        gameData = new GameData();
        playerRef = GameObject.Find("Player");
    }
    private void Update()
    {
        if (shouldRead)
        {
            jsonLoad();
            shouldRead = false;
        }

        if (shouldWrite)
        {
            jsonSave();
            shouldWrite = false;
        }
        
    }


    static void WriteString()
    {
        GameObject playerRef = GameObject.Find("Player");
        int currentGunIndex = playerRef.GetComponent<PlayerShooting>().GetCurrentGunIndex();

        string filePath;
        if (!Application.isEditor)
        {
         filePath = "GunCreator_Data/gun" + currentGunIndex + ".txt";
        }
        else
        {
          filePath = "Assets/Resources/gun" + currentGunIndex + ".txt";
        }

        //Write some text to the test.txt file
        StreamWriter writer = new StreamWriter(filePath, true);
        writer.WriteLine("Test");
        writer.Close();

        //Re-import the file to update the reference in the editor
        //AssetDatabase.ImportAsset(path);
       // Resources.Load(path);
        TextAsset asset = (TextAsset)Resources.Load("test");

        //Print the text from the file
        Debug.Log(asset.text);
    }


    static void ReadString()
    {
        GameObject playerRef = GameObject.Find("Player");
        int currentGunIndex = playerRef.GetComponent<PlayerShooting>().GetCurrentGunIndex();

        string filePath;
        if (!Application.isEditor)
        {
            filePath = "GunCreator_Data/gun" + currentGunIndex + ".txt";
        }
        else
        {
            filePath = "Assets/Resources/gun" + currentGunIndex + ".txt";
        }

        //Read the text from directly from the test.txt file
        StreamReader reader = new StreamReader(filePath);
        Debug.Log(reader.ReadLine());
        //Debug.Log();
        string me = reader.ReadLine();
        print(me);
       // float numbers = float.Parse(reader.ReadLine());
       // print(numbers);
        reader.Close();
    }

    void jsonSave()
    {
        gameData.tempBulletLifetime = playerRef.GetComponent<PlayerShooting>().GetCurrentGun().bulletLifetime;
        gameData.tempBulletSpeed = playerRef.GetComponent<PlayerShooting>().GetCurrentGun().bulletSpeed; 
        gameData.tempAmmoPerClip = playerRef.GetComponent<PlayerShooting>().GetCurrentGun().ammoPerClip;
        
        gameData.tempKickback = playerRef.GetComponent<PlayerShooting>().GetCurrentGun().kickbackForcePerShot;
        gameData.tempShotsPerSecond = playerRef.GetComponent<PlayerShooting>().GetCurrentGun().shotsPerSecond;
        gameData.tempReloadTime = playerRef.GetComponent<PlayerShooting>().GetCurrentGun().reloadTime; 
        gameData.tempSpeedLimitPercentage = playerRef.GetComponent<PlayerShooting>().GetCurrentGun().speedLimitPercentage; 
        gameData.tempGunshotPitch = playerRef.GetComponent<PlayerShooting>().GetCurrentGun().gunshotPitch;
        gameData.tempIsExplosiveAmmo = playerRef.GetComponent<PlayerShooting>().GetCurrentGun().isExplosiveAmmo;
        gameData.tempDoesDespawnOnHit = playerRef.GetComponent<PlayerShooting>().GetCurrentGun().doesDespawnOnHit;
        gameData.tempIsAutomatic = playerRef.GetComponent<PlayerShooting>().GetCurrentGun().isAutomatic;

        string dataAsJson = JsonUtility.ToJson(gameData, true);

        int currentGunIndex = playerRef.GetComponent<PlayerShooting>().GetCurrentGunIndex();

        string filePath;
        if (!Application.isEditor)
        {
            filePath = "GunCreator_Data/gun" + currentGunIndex + ".txt";
        }
        else
        {
            filePath = "Assets/Resources/gun" + currentGunIndex + ".txt";
        }
        File.WriteAllText(filePath, dataAsJson);
      //  ReadString();


    }

    void jsonLoad()
    {
        int currentGunIndex = playerRef.GetComponent<PlayerShooting>().GetCurrentGunIndex();
        string filePath;
        if (!Application.isEditor)
        {
            filePath = "GunCreator_Data/gun" + currentGunIndex + ".txt";
        }
        else
        {
            filePath = "Assets/Resources/gun" + currentGunIndex + ".txt";
        }

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            gameData = JsonUtility.FromJson<GameData>(dataAsJson);
            print(dataAsJson);
        }
        else
        {
            gameData = new GameData();
        }

        Gun playerCurrentGun = playerRef.GetComponent<PlayerShooting>().GetCurrentGun();
        //going to have to update every thing here, as well as make sure that we have 9 files (1 for each gun)
      

        playerCurrentGun.setBulletLifetime(gameData.tempBulletLifetime);
        playerCurrentGun.setBulletSpeed (gameData.tempBulletSpeed);
        playerCurrentGun.setAmmoPerClip(gameData.tempAmmoPerClip);


        playerCurrentGun.setKickbackPerShot(gameData.tempKickback);
        playerCurrentGun.setShotsPerSecond(gameData.tempShotsPerSecond);
        playerCurrentGun.setReloadTime(gameData.tempReloadTime);
        playerCurrentGun.setSpeedLimitPercentage(gameData.tempSpeedLimitPercentage);
        playerCurrentGun.setGunshotPitch(gameData.tempGunshotPitch);
        playerCurrentGun.setExplosiveAmmo(gameData.tempIsExplosiveAmmo);
        playerCurrentGun.setDoesDespawnOnHit(gameData.tempDoesDespawnOnHit);
        playerCurrentGun.setIsAuto(gameData.tempIsAutomatic);


        print("loading");
    }
    public void funcSave()
    {
        shouldWrite = true;
    }
    public void funcLoad()
    {
        shouldRead = true;
    }

    public void ForceLoad()
    {
        jsonLoad();
    }
}
