Nick Magnus Milestone 1 Read-Me


Quick Controls
WASD - Standard 2D Movement
LMB - Shoot towards Mouse

Milestone Goals:

1. Base player mechanics implemented (Moving, shooting a basic gun, basic camera smoothing)
	a. They are not as polished as I want them to be, but the player can move and shoot, and there is a simple camera smoothing 
		algorithm in place.


2. Starting on enemies (Health, Pathing, Spawning, shooting)
	a. I wanted get some actual pathfinding/ai going on, but we had to go with something simpler due to time on this milestone.
		I hope to expand it in the future

3. Paper Design & Answers to questions posed in first peer review
	a. See other documents in this folder